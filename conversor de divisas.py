

def foreign_exchage_calculator(ammount):
    dolar_to_euro_rate = 0.84

    return dolar_to_euro_rate * ammount


def run():
    print('C A L C U L A D O R A  D E  D I V I S A S')
    print('convierte dolares a euros.')
    print('')

    ammount = float(input('ingresa la cantidad dolares que quieres convertir.'))

    result = foreign_exchage_calculator(ammount)

    print('${} dolares son ${} euros.'.format(ammount, result))
    print('')

if __name__ == '__main__':
    run()
